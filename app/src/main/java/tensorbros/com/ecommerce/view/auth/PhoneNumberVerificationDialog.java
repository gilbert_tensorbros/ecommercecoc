package tensorbros.com.ecommerce.view.auth;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tensorbros.com.ecommerce.R;
import tensorbros.com.ecommerce.custom_classes.CustomDialogFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhoneNumberVerificationDialog extends CustomDialogFragment {


    private View view;

    public PhoneNumberVerificationDialog() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_phone_number_verification_dialog, container, false);

        // Inflate the layout for this fragment
        return view;
    }

}
