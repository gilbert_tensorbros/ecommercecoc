package tensorbros.com.ecommerce.view.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;

import com.google.firebase.auth.FirebaseAuth;

import tensorbros.com.ecommerce.MainActivity;
import tensorbros.com.ecommerce.R;
import tensorbros.com.ecommerce.data_access.DA;
import tensorbros.com.ecommerce.data_access.UserDA;
import tensorbros.com.ecommerce.model.RequestCodes;
import tensorbros.com.ecommerce.model.User;
import tensorbros.com.ecommerce.presenter.GoogleAuthP;



public class AuthActivity extends AppCompatActivity
        implements GoogleAuthP.GoogleAuthListener, UserDA.UserDaCallback {

    private CardView googleBtn;
    private GoogleAuthP googleAuthP;
    private FirebaseAuth firebaseAuth;
    private UserDA userDA;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseAuth = FirebaseAuth.getInstance();
        userDA = new UserDA();
        userDA.setUserDaCallback(this);

        if (firebaseAuth.getCurrentUser() != null) {
            redirectToMain();
        }

        setContentView(R.layout.activity_auth);
        getSupportActionBar().hide();

        googleAuthP = new GoogleAuthP(this, firebaseAuth);
        googleAuthP.setGoogleAuthCallback(this);

        googleBtn = findViewById(R.id.googleBtn);
        googleBtn.setOnClickListener(googleAuthP);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RequestCodes.RC_G_SIGNIN) {
            googleAuthP.loginToGoogle(data);
        }
    }

    @Override
    public void onGoogleSignIn(User user) {
        this.user = user;
        userDA.QueryUserOnce(firebaseAuth.getCurrentUser().getUid());
    }

    @Override
    public void onFailure() {
        new DA().log("LOGIN FAILED");
    }

    private void redirectToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onUserAdded() {
        redirectToMain();
    }

    @Override
    public void onUserQueriedOnce(User u) {
        if(u == null) {
            userDA.addUser(firebaseAuth.getCurrentUser().getUid(), user);
        }
        else {
            redirectToMain();
        }
    }
}
