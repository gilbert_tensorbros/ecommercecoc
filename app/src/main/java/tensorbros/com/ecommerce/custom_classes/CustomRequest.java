package tensorbros.com.ecommerce.custom_classes;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by John on 11/12/2017.
 */

public class CustomRequest extends StringRequest {

    public CustomRequest(int method, String url, Response.Listener<String> listener,
                         Response.ErrorListener errorListener, long number, String message) {
        super(method, url, listener, errorListener);
    }

    @Override
    public Map<String, String> getParams() throws AuthFailureError {

        Map<String, String> map = new HashMap<>();
        map.put("message", "CLASH TEST");
        map.put("number", "09162591352");

        return map;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> map = new HashMap<>();

        map.put("accept", "text/html");
        return map;
    }
}
