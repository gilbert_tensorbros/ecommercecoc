package tensorbros.com.ecommerce.utils;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import tensorbros.com.ecommerce.custom_classes.CustomRequest;

public class SmsUtil {

    private SmsUtilCallback smsUtilCallback;

    public void sendSms(long number, String message, Context context) {

        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "https:/sms-test.bayadcenter.net/sms/push";

        CustomRequest stringRequest = new CustomRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        smsUtilCallback.onSmsSent(true);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }, number, message);
        queue.add(stringRequest);
    }


    public interface SmsUtilCallback {
        void onSmsSent(boolean success);
    }


}
