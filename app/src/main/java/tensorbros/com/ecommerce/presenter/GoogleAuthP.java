package tensorbros.com.ecommerce.presenter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import tensorbros.com.ecommerce.data_access.DA;
import tensorbros.com.ecommerce.model.RequestCodes;
import tensorbros.com.ecommerce.model.User;



public class GoogleAuthP implements View.OnClickListener {

    private GoogleSignInOptions gso;
    private GoogleSignInClient signInClient;
    private Activity activity;
    private FirebaseAuth auth;
    private GoogleAuthListener googleAuthListener;
    private final String CLIENT_ID = "715730985396-qcrdq796authqpd95r8hs7sj6kt4jc72.apps.googleusercontent.com";

    public GoogleAuthP(Activity activity, FirebaseAuth auth) {
        this.activity = activity;
        this.auth = auth;

        // Configure Google Sign In
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(CLIENT_ID)
                .requestEmail()
                .build();

        signInClient = GoogleSignIn.getClient(activity, gso);
    }

    @Override
    public void onClick(View v) {
        Intent signInIntent = signInClient.getSignInIntent();
        activity.startActivityForResult(signInIntent, RequestCodes.RC_G_SIGNIN);
    }

    public void loginToGoogle(Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        Log.d(null, result.getStatus().toString());

        if (result.isSuccess()) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                final GoogleSignInAccount account = task.getResult(ApiException.class);

                AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
                auth.signInWithCredential(credential)
                        .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                new DA().log(task.getResult().getUser().toString());
                                if (task.isSuccessful()) {

                                    User u = new User();
                                    u.setFirst_name(account.getGivenName());
                                    u.setLast_name(account.getFamilyName());
                                    u.setDp_url(account.getPhotoUrl().toString());
                                    googleAuthListener.onGoogleSignIn(u);


                                } else {
                                    googleAuthListener.onFailure();
                                    task.getException().printStackTrace();
                                }
                            }
                        });
            } catch (ApiException e) {
                e.printStackTrace();
            }
        }
        else {
            googleAuthListener.onFailure();
        }
    }


    public interface GoogleAuthListener {
        void onGoogleSignIn(User user);
        void onFailure();
    }

    public void setGoogleAuthCallback(GoogleAuthListener googleAuthListener) {
        this.googleAuthListener = googleAuthListener;
    }
}
