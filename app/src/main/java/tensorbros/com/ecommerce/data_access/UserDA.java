package tensorbros.com.ecommerce.data_access;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import tensorbros.com.ecommerce.model.User;


public class UserDA extends DA{

    /*
    Created on 09/27/2018 by Gilbert
    User Data Access
    Adds and reads user accounts
     */

    private DatabaseReference ref = database.getReference("users");

    private UserDaCallback userDaCallback;

    public void addUser(String key, User user) {
        ref.child(key).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if (userDaCallback != null)
                    userDaCallback.onUserAdded();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    public void QueryUserOnce(String key) {
        new DA().log(ref.getKey());
        ref.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                new DA().log(user);
                userDaCallback.onUserQueriedOnce(user);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public interface UserDaCallback {
        void onUserAdded();
        void updateComplete();
        void onUserQueriedOnce(User user);
    }

    public UserDaCallback getUserDaCallback() {
        return userDaCallback;
    }

    public void setUserDaCallback(UserDaCallback userDaCallback) {
        this.userDaCallback = userDaCallback;
    }
}
