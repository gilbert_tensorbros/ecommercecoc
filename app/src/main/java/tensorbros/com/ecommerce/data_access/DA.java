package tensorbros.com.ecommerce.data_access;

import com.google.firebase.database.FirebaseDatabase;

public class DA {

    public FirebaseDatabase database = FirebaseDatabase.getInstance();

    public void log(Object o) {
        database.getReference("logs").push().setValue(o);
    }
}
